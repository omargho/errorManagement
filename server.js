'use strict';

const bodyParser = require('body-parser');
const cors = require('cors');
const express = require('express');
const mongoose = require('mongoose');
const config = require('./config');
const helmet = require('helmet');
const app = module.exports = express();

mongoose.connect(config.mongodb, {
  useMongoClient: true
});
mongoose.Promise = global.Promise;

app.set('port', config.port);
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(helmet());

/** routes **/

const version = config.apiVersion ? '/' + config.apiVersion : '';

app.use(version + '/hello', require('./app/controllers/hello'));
app.use(version + '/user', require('./app/controllers/user'));
app.use(version + '/application', require('./app/controllers/application'));

/** Middleware **/

app.use((err, req, res, next) => {

  if (err && err.status) {
    res.status(err.status).send({message: err.message});
  }
  else next(err);
});

/** Display uncaught Exception. **/

process.on('uncaughtException', (err) => {
  console.log('--------------------------------------', err);
  console.log('error', (new Date()).toUTCString() + ' uncaughtException:', err.message);
  console.log('error', err.stack);
  process.exit(1);
});

/** starting the server **/

if (!module.parent) {
  app.listen(app.get('port'), function () {
    console.log('listening on port: ', app.get('port'));
    console.log('ENV: ', config.env);
  });
}
