'use strict';

const Application = require('../../models/application');

const create = function (req, res, next) {
  if (!req.body.name) throw {status: 401, message: 'name is missing'}
  if (!req.body.description) throw {status: 401, message: 'description is missing'}

  const app = {
    name: req.body.name,
    description: req.body.description,
  };
  const currentUserId = req.body.current.id;

  const theApp = new Application(app);
  theApp
  .create(currentUserId)
  .then(doc => {
    return res.send(doc);
  })
  .catch(next);
};

const deleteApp = function (req, res, next) {
  const id = req.body._id
  const userId = req.body.current.id;
  Application
  .findOne({_id: id})
  .then((doc, err) => {
    if (doc.admin != userId)
      throw {status: 401, message: 'only admin can delete the app'};
    return doc.remove();
  }).then(() =>
    res.status(204).send({})
  ).catch(next);
}

module.exports = {
  create,
  deleteApp,
};
