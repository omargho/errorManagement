'use strict';

const applicationController = require('./application.controller');
const express = require('express');
const router = express.Router();
const jwt = require('../../services/jwt');

router.post('/', jwt.userIsAuthenticated, applicationController.create);
module.exports = router;
