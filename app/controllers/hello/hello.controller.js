'use strict';

const firebase = require('../../services/firebase');


const hello = function (req, res) {
  const registrationToken = 'f0NqW6SEoCI:APA91bHg2GXCl4yoNT7N7hVMlNoc65M5ofYyS5vC6P2Nh2Zdgbi5g0jiM2fZHPtXoizpPfet_BqBpm3jXshoNGW-qjjleH6HhCViJCdE0fN3fYcJURvs_-FjrdJYZIE_PgQF4L5I1IRT';

  // See the "Defining the message payload" section below for details
  // on how to define a message payload.
  const payload = {
    notification: {
      //badge:"badge",
      //color:"#rrggbb",
      sound: 'alarm',
      title: 'aaa',
      body: 'babab'
    }
  };
  const options = {
    priority: 'high',
    'mutable-content': true,
    timeToLive: 60 * 60 * 24
  };

  // Send a message to the device corresponding to the provided
  // registration token.
  firebase.messaging().sendToDevice(registrationToken, payload, options)
    .then(function (response) {
      // See the MessagingDevicesResponse reference documentation for
      // the contents of response.
      //console.log('Successfully sent message:', JSON.stringify(response));
      res.send({text: 'hello world'});
    })
    .catch(function (error) {
      // console.log('Error sending message:', JSON.stringify(error));
    });

};

module.exports = {
  hello
};
