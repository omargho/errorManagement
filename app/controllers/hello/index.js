'use strict';

const helloController = require('./hello.controller');
const express = require('express');
const router = express.Router();

router.get('/', helloController.hello);
module.exports = router;
