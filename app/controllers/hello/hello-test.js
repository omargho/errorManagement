process.env.NODE_ENV = 'test';

const app = require('../../server');
const request = require('supertest').agent(app.listen());
const expect = require('chai').expect;

describe('ENDPOINT /hello', () => {


  before(done => {
    done();
  });

  after(done => {
    done();
  });


  describe('test - GET /hello', () => {

    it('should return hello world - status_code: 200', (done) => {
      request.get('/hello')
        .expect(200)
        .end((err, req) => {
          if (err) return done(err);
          expect(req.body.text).to.equal('hello world');
          done();
        });
    });
  });
});
