'use strict';

const clientController = require('./client.controller');
const express = require('express');
const router = express.Router();

router.post('/register', clientController.register);
router.post('/authenticate', clientController.authenticate);
module.exports = router;
