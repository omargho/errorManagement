'use strict';

const firebase = require('firebase-admin');
const firebaseAccount = require('../../config/firebase.json');

firebase.initializeApp({
  credential : firebase.credential.cert(firebaseAccount),
  databaseURL : 'https://error-notif.firebaseio.com/'
});

module.exports = firebase;
