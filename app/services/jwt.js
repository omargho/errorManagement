'use strict';

const jwt = require('jsonwebtoken');
const config = require('../../config');

function userIsAuthenticated(req, res, next) {

  if (!(req.headers && req.headers.authorization)) {
    return res.status(400).send({
      success : false,
      message : 'You did not provide a JSON Web Token in the Authorization header'
    });
  }

  const token = req.headers.authorization.split(' ')[1];
  let payload = null;

  try {
    payload = jwt.verify(token, config.APP_SECRET);
  } catch (err) {
    return res.status(401).send({success : false, message : 'Invalid token'});
  }

  req.body.current = payload;
  next();
}

function createJWT(id) {

  const payload = {
    id
  };

  return jwt.sign(payload, config.APP_SECRET, {expiresIn : '24h'});
}

module.exports = {
  userIsAuthenticated,
  createJWT
};
