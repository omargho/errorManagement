'use strict';

const Schema = require('./schema.js');
const crypto = require('crypto');
const jwt = require('../../services/jwt');

Schema.methods = {

  register() {

    this.salt = crypto.randomBytes(16).toString('base64');
    this.password = crypto.pbkdf2Sync(this.password, new Buffer(this.salt, 'base64'), 10000, 64, 'sha1').toString('hex');
    return new Promise((resolve, reject) => {
      this
      .save()
      .then(doc => {
        resolve({email: doc.email, name: doc.name, fcmToken: doc.fcmToken, _id: doc._id});
      })
      .catch(() => reject({status: 409, message: 'email already used'}));
    });
  },

  authenticate() {

    return new Promise((resolve, reject) => {
      this.model('User')
      .findOne({email: this.email})
      .then(doc => {
        const isInalidPassword = crypto.pbkdf2Sync(this.password, Buffer(doc.salt, 'base64'), 10000, 64, 'sha1').toString('hex') !== doc.password;
        if (isInalidPassword) throw {};
        resolve({token: jwt.createJWT(doc._id)});
      })
      .catch(() => reject({status: 401, message: 'The email/password combinaison is invalid'}));
    });
  }
};
