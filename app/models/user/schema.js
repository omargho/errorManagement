'use strict';

const mongoose = require('mongoose');

const Schema = new mongoose.Schema({

  email: {
    type: String,
    unique: true,
    required: true
  },
  name: {
    type: String,
    required: true
  },
  fcmToken: {
    type: String,
    unique: true
  },
  salt: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  adminOfApps: [{type: mongoose.Schema.Types.ObjectId, ref: 'Application'}],
  memberOfApps: [{type: mongoose.Schema.Types.ObjectId, ref: 'Application'}]

});

module.exports = Schema;
