'use strict';

const Schema = require('./schema.js');
const crypto = require('crypto');

Schema.methods = {

  create (userId) {
    this.appSecret = crypto.randomBytes(30).toString('hex');
    return new Promise((resolve, reject) => {
      this.model('User')
        .findOne({_id : userId})
        .then(user => {
          user.adminOfApps.push(this._id);
          user.memberOfApps.push(this._id);
          return user.save();
        })
        .then((user) => {
          this.admin = user._id;
          this.usersToNotify.push(user._id);
          this.appId = this._id;
          return this.save();
        })
        .then((application) => {
          resolve(application);
        })
        .catch(err => reject(err));
    });
  }

};
