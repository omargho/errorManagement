'use strict';

const mongoose = require('mongoose');
const Schema = require('./schema.js');
const methods = require('./methods.js'); // eslint-disable-line no-unused-vars
const timestamps = require('mongoose-timestamp');

Schema.plugin(timestamps);

module.exports = mongoose.model('Application', Schema);
