'use strict';

const mongoose = require('mongoose');

const Schema = new mongoose.Schema({

  name : {
    type : String,
    required : true
  },
  description : {
    type : String,
    default : ''
  },
  appId : {
    type : String,
    required : true
  },
  appSecret : {
    type : String,
    required : true
  },
  admin : {type : mongoose.Schema.Types.ObjectId, ref : 'User'},
  usersToNotify : [{type : mongoose.Schema.Types.ObjectId, ref : 'User'}]

});

module.exports = Schema;
