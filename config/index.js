'use strict';

process.env.NODE_ENV = process.env.NODE_ENV || 'development';

//All configurations will extend these options
// ============================================
const all = {
  mongodb : process.env.MONGO_HOST || 'mongodb://localhost/errory',
  env : process.env.NODE_ENV,
  port : process.env.PORT || 3000,
  APP_SECRET : 'helloOmar'
};

// Export the config object based on the NODE_ENV
// ==============================================
module.exports = all;
